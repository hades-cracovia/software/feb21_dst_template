

#include "hparticlecand.h"
#include "hcategorymanager.h"
#include "hcategory.h"

#include "hparticletree.h"
#include "hparticleevtinfo.h"
#include "htime.h"

#include "hparticledef.h"
#include "hstartdef.h"
#include "walldef.h"
#include "richdef.h"
#include "showerdef.h"
#include "hstartdef.h"
#include "tofdef.h"
#include "rpcdef.h"
#include "hmdcdef.h"
#include "hmdctrackddef.h"
#include "hmdctrackgdef.h"


#include "TObjArray.h"
#include "TSystem.h"


Bool_t selectEventTreeFilterAccepted(TObjArray* ar){

    HCategory* evtInfoCat = (HCategory*)HCategoryManager::getCategory(catParticleEvtInfo);
    HParticleEvtInfo* evtInfo=0;
    evtInfo = HCategoryManager::getObject(evtInfo,evtInfoCat,0 );

    if(evtInfo&&!evtInfo->isGoodEvent(Particle::kGoodTRIGGER|          // standard event selection apr12
				     Particle::kGoodVertexClust|
				     Particle::kGoodVertexCand|
				     Particle::kGoodSTART|
				     //Particle::kNoPileUpSTART|
				     Particle::kNoSTART|
				     Particle::kNoVETO|
				     Particle::kGoodSTARTVETO|
				     Particle::kGoodSTARTMETA
				    ))
    {
	return kFALSE;
    }

    return kTRUE;
}

void addFilterAccepted(HTaskSet *masterTaskSet, TString inFile,TString outDir)
{

    TString dir    = gSystem->DirName (inFile);
    //TString file   = HTime::stripFileName(inFile);
    TString file   = gSystem->BaseName (inFile);

    TString fileWoPath = file;
    fileWoPath.ReplaceAll(".hld","");
    Int_t evtBuild = HTime::getEvtBuilderFileName(fileWoPath,kFALSE);

    HParticleTree* parttree = new HParticleTree("particletree_accepted","particletree_accepted");


//       SUPPORTED CATEGORIES:
//
//       catParticleCand
//       catParticleEvtInfo (fullcopy)
//       catStart2Hit       (fullcopy)
//       catStart2Cal       (fullcopy)
//       catTBoxChan        (fullcopy)
//       catWallHit         (fullcopy)
//       catTofHit          (opt full)
//       catTofCluster      (opt full)
//       catRpcCluster      (opt full)
//       catShowerHit       (opt full)
//       catRichHit         (opt full)
//       catRichDirClus     (fullcopy)
//       catRichCal         (fullcopy)
//       catMdcSeg
//       catMdcHit
//       catMdcCal1
//       catMdcClus
//       catMdcClusInf
//       catMdcClusFit
//       catMdcWireFit
//       catGeantKine             (fullcopy)
//       catMdcGeantRaw           (fullcopy)
//       catTofGeantRaw           (fullcopy)
//       catRpcGeantRaw           (fullcopy)
//       catShowerGeantRaw        (fullcopy)
//       catWallGeantRaw          (fullcopy)
//       catRichGeantRaw (+1,+2)  (fullcopy)


    //------Long-List-Of-Categories-which-are-needed-in-this-DST-production------//
    //----------------------------------------------------------------------------//
    Cat_t pPersistentCatAll[] =
    {
	//catRichDirClus,catRichHit, catRichCal,  // full copy
	//catMdcCal1,
	//catShowerHit,
	//catTofHit, catTofCluster,
	//catRpcHit, catRpcCluster,


	catParticleCand, catParticleEvtInfo,catEmcCluster,catEmcCal,catWallEventPlane
    };

    //------Short-List-Of-Categories-which-are-needed-in-this-DST-production------//
    //----------------------------------------------------------------------------//
    Cat_t pPersistentCat[] =
    {
	//catRichDirClus,catRichHit, catRichCal,  // full copy
	//catMdcCal1,
	//catShowerHit,
	//catTofHit, catTofCluster,
	//catRpcHit, catRpcCluster,

	catParticleCand, catParticleEvtInfo,catEmcCluster,catEmcCal,catWallEventPlane
    };

    //------lsit of full copy categories------------------------------------------//
    //----------------------------------------------------------------------------//
    Cat_t pPersistentCatFull[] =
    {
	//catRichDirClus,
	catRichHit,catRichCal,catWallHit,catStart2Cal, catStart2Hit,catTBoxChan
    };


    if(evtBuild == 1) {
	parttree->setEventStructure(sizeof(pPersistentCatAll)/sizeof(Cat_t) ,pPersistentCatAll);
        parttree->setEventStructure(sizeof(pPersistentCatFull)/sizeof(Cat_t),pPersistentCatFull,kTRUE);     // add more
    } else {
	parttree->setEventStructure(sizeof(pPersistentCat)/sizeof(Cat_t)    ,pPersistentCat);
        parttree->setEventStructure(sizeof(pPersistentCatFull)/sizeof(Cat_t),pPersistentCatFull,kTRUE);
    }
    parttree->setSkipEmptyEvents(kTRUE);
    parttree->setSkipTracks(kFALSE);
    parttree->setDoSorter(kFALSE);

    parttree->setSortType(Particle::kIsBestRKSorter);

    TString outfile = Form("%s/%s_dst_mar19_accepted.root",outDir.Data(),file.Data());
    outfile.ReplaceAll("//","/");
    parttree->setUserSelectionEvent(selectEventTreeFilterAccepted,NULL);
    parttree->setOutputFile(outfile,"Filter","RECREATE",2 );
    masterTaskSet->add(parttree);
}












