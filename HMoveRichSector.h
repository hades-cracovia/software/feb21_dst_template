
#ifndef __HMoveRichSector___


#include "hreconstructor.h"
#include "hcategorymanager.h"
#include "hcategory.h"

//--------category definitions---------
#include "richdef.h"
//-------------------------------------

//-------objects-----------------------
#include "hrichhit.h"
//-------------------------------------

//-------------------------------------
// conatiners
#include "hrich700digipar.h"
//-------------------------------------


#include <iostream>
#include <cstdlib>
using namespace std;


class HMoveRichSector : public HReconstructor
{

  HRich700DigiPar* digipar;

public:

    HMoveRichSector(const Text_t *name = "",const Text_t *title ="") {
    }
    virtual ~HMoveRichSector() {;}

    Bool_t init()
    {

        digipar = (HRich700DigiPar*) gHades->getRuntimeDb()->getContainer("Rich700DigiPar");
	return kTRUE;
    }

    Int_t execute()
    {
	// -------------------------------------------------------------------------
	HCategory* fCatRichHit = HCategoryManager::getCategory(catRichHit,kTRUE,"catRichHit");
	if(!fCatRichHit) { cout<<"NO catRichHit!"<<endl; exit(1);}
	// -------------------------------------------------------------------------


	// -------------------------------------------------------------------------
	HRichHit* richHit;

	Int_t n = fCatRichHit->getEntries();
	for(Int_t i = 0; i < n; i ++){
	    richHit = HCategoryManager::getObject(richHit,fCatRichHit,i);
	    if(richHit){

		Int_t s = richHit->getSector();
		if(s != 4) richHit->setPhi(richHit->getPhi()+60 );
                else      richHit->setPhi(richHit->getPhi()-300);

		if(s != 4) richHit->setMeanPhi(richHit->getMeanPhi()+60 );
                else       richHit->setMeanPhi(richHit->getMeanPhi()-300);


		// rotate sector by one
		if(s < 5) s+= 1;
                else      s = 0;
		richHit->setSector(s);


		// for rich700 x,y has to be manipulated
                // since it will be used later
		Float_t phi1 = richHit->getPhi();
		Float_t the1 = richHit->getTheta();
		Float_t x1,y1;
		digipar->getRingCenterXY(the1, phi1,0,x1,y1);
                richHit->setXY(x1,y1);
                richHit->setRich700CircleCenterX(x1);
                richHit->setRich700CircleCenterY(y1);

	    }
	} // loop richit
	// -------------------------------------------------------------------------


	return 0;
    }

    Bool_t finalize()
    {
	return kTRUE;
    }

};
#endif
