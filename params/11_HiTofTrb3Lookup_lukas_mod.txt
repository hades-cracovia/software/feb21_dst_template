##############################################################################
# Lookup table for the TRB3 unpacker of the iTOF detector
# Format:
# trbnet-address  channel sector  module  cell slot
##############################################################################
[iTofTrb3Lookup]
//----------------------------------------------------------------------------
0x5d00    1    0    0    0    0
0x5d00    2    0    0    0    1
0x5d00    3    0    0    0    2
0x5d00    4    0    0    0    3
0x5d00    5    0    0    0    4
0x5d00    6    0    0    0    5
0x5d00    7    0    0    0    6
0x5d00    8    0    0    0    7
0x5d00    9    0    0    0    8
0x5d00   10    0    0    0    9
0x5d00   11    0    0    0   10
0x5d00   12    0    0    0   11
0x5d00   13    0    0    0   12
0x5d00   14    0    0    0   13
0x5d00   15    0    0    0   14
0x5d00   16    0    0    0   15
0x5d00   17    0    0    1    0
0x5d00   18    0    0    1    1
0x5d00   19    0    0    1    2
0x5d00   20    0    0    1    3
0x5d00   21    0    0    1    4
0x5d00   22    0    0    1    5
0x5d00   23    0    0    1    6
0x5d00   24    0    0    1    7
0x5d00   25    0    0    1    8
0x5d00   26    0    0    1    9
0x5d00   27    0    0    1   10
0x5d00   28    0    0    1   11
0x5d00   29    0    0    1   12
0x5d00   30    0    0    1   13
0x5d00   31    0    0    1   14
0x5d00   32    0    0    1   15
0x5d00   33    0    0    2    0
0x5d00   34    0    0    2    1
0x5d00   35    0    0    2    2
0x5d00   36    0    0    2    3
0x5d00   37    0    0    2    4
0x5d00   38    0    0    2    5
0x5d00   39    0    0    2    6
0x5d00   40    0    0    2    7
0x5d00   41    0    0    2    8
0x5d00   42    0    0    2    9
0x5d00   43    0    0    2   10
0x5d00   44    0    0    2   11
0x5d00   45    0    0    2   12
0x5d00   46    0    0    2   13
0x5d00   47    0    0    2   14
0x5d00   48    0    0    2   15
0x5d01    1    1    0    0    0
0x5d01    2    1    0    0    1
0x5d01    3    1    0    0    2
0x5d01    4    1    0    0    3
0x5d01    5    1    0    0    4
0x5d01    6    1    0    0    5
0x5d01    7    1    0    0    6
0x5d01    8    1    0    0    7
0x5d01    9    1    0    0    8
0x5d01   10    1    0    0    9
0x5d01   11    1    0    0   10
0x5d01   12    1    0    0   11
0x5d01   13    1    0    0   12
0x5d01   14    1    0    0   13
0x5d01   15    1    0    0   14
0x5d01   16    1    0    0   15
0x5d01   17    1    0    1    0
0x5d01   18    1    0    1    1
0x5d01   19    1    0    1    2
0x5d01   20    1    0    1    3
0x5d01   21    1    0    1    4
0x5d01   22    1    0    1    5
0x5d01   23    1    0    1    6
0x5d01   24    1    0    1    7
0x5d01   25    1    0    1    8
0x5d01   26    1    0    1    9
0x5d01   27    1    0    1   10
0x5d01   28    1    0    1   11
0x5d01   29    1    0    1   12
0x5d01   30    1    0    1   13
0x5d01   31    1    0    1   14
0x5d01   32    1    0    1   15
0x5d01   33    1    0    2    0
0x5d01   34    1    0    2    1
0x5d01   35    1    0    2    2
0x5d01   36    1    0    2    3
0x5d01   37    1    0    2    4
0x5d01   38    1    0    2    5
0x5d01   39    1    0    2    6
0x5d01   40    1    0    2    7
0x5d01   41    1    0    2    8
0x5d01   42    1    0    2    9
0x5d01   43    1    0    2   10
0x5d01   44    1    0    2   11
0x5d01   45    1    0    2   12
0x5d01   46    1    0    2   13
0x5d01   47    1    0    2   14
0x5d01   48    1    0    2   15
0x5d02    1    2    0    0    0
0x5d02    2    2    0    0    1
0x5d02    3    2    0    0    2
0x5d02    4    2    0    0    3
0x5d02    5    2    0    0    4
0x5d02    6    2    0    0    5
0x5d02    7    2    0    0    6
0x5d02    8    2    0    0    7
0x5d02    9    2    0    0    8
0x5d02   10    2    0    0    9
0x5d02   11    2    0    0   10
0x5d02   12    2    0    0   11
0x5d02   13    2    0    0   12
0x5d02   14    2    0    0   13
0x5d02   15    2    0    0   14
0x5d02   16    2    0    0   15
0x5d02   17    2    0    1    0
0x5d02   18    2    0    1    1
0x5d02   19    2    0    1    2
0x5d02   20    2    0    1    3
0x5d02   21    2    0    1    4
0x5d02   22    2    0    1    5
0x5d02   23    2    0    1    6
0x5d02   24    2    0    1    7
0x5d02   25    2    0    1    8
0x5d02   26    2    0    1    9
0x5d02   27    2    0    1   10
0x5d02   28    2    0    1   11
0x5d02   29    2    0    1   12
0x5d02   30    2    0    1   13
0x5d02   31    2    0    1   14
0x5d02   32    2    0    1   15
0x5d02   33    2    0    2    0
0x5d02   34    2    0    2    1
0x5d02   35    2    0    2    2
0x5d02   36    2    0    2    3
0x5d02   37    2    0    2    4
0x5d02   38    2    0    2    5
0x5d02   39    2    0    2    6
0x5d02   40    2    0    2    7
0x5d02   41    2    0    2    8
0x5d02   42    2    0    2    9
0x5d02   43    2    0    2   10
0x5d02   44    2    0    2   11
0x5d02   45    2    0    2   12
0x5d02   46    2    0    2   13
0x5d02   47    2    0    2   14
0x5d02   48    2    0    2   15
0x5d03    1    3    0    0    0
0x5d03    2    3    0    0    1
0x5d03    3    3    0    0    2
0x5d03    4    3    0    0    3
0x5d03    5    3    0    0    4
0x5d03    6    3    0    0    5
0x5d03    7    3    0    0    6
0x5d03    8    3    0    0    7
0x5d03    9    3    0    0    8
0x5d03   10    3    0    0    9
0x5d03   11    3    0    0   10
0x5d03   12    3    0    0   11
0x5d03   13    3    0    0   12
0x5d03   14    3    0    0   13
0x5d03   15    3    0    0   14
0x5d03   16    3    0    0   15
0x5d03   17    3    0    1    0
0x5d03   18    3    0    1    1
0x5d03   19    3    0    1    2
0x5d03   20    3    0    1    3
0x5d03   21    3    0    1    4
0x5d03   22    3    0    1    5
0x5d03   23    3    0    1    6
0x5d03   24    3    0    1    7
0x5d03   25    3    0    1    8
0x5d03   26    3    0    1    9
0x5d03   27    3    0    1   10
0x5d03   28    3    0    1   11
0x5d03   29    3    0    1   12
0x5d03   30    3    0    1   13
0x5d03   31    3    0    1   14
0x5d03   32    3    0    1   15
0x5d03   33    3    0    2    0
0x5d03   34    3    0    2    1
0x5d03   35    3    0    2    2
0x5d03   36    3    0    2    3
0x5d03   37    3    0    2    4
0x5d03   38    3    0    2    5
0x5d03   39    3    0    2    6
0x5d03   40    3    0    2    7
0x5d03   41    3    0    2    8
0x5d03   42    3    0    2    9
0x5d03   43    3    0    2   10
0x5d03   44    3    0    2   11
0x5d03   45    3    0    2   12
0x5d03   46    3    0    2   13
0x5d03   47    3    0    2   14
0x5d03   48    3    0    2   15
0x5d04    1    4    0    0    0
0x5d04    2    4    0    0    1
0x5d04    3    4    0    0    2
0x5d04    4    4    0    0    3
0x5d04    5    4    0    0    4
0x5d04    6    4    0    0    5
0x5d04    7    4    0    0    6
0x5d04    8    4    0    0    7
0x5d04    9    4    0    0    8
0x5d04   10    4    0    0    9
0x5d04   11    4    0    0   10
0x5d04   12    4    0    0   11
0x5d04   13    4    0    0   12
0x5d04   14    4    0    0   13
0x5d04   15    4    0    0   14
0x5d04   16    4    0    0   15
0x5d04   17    4    0    1    0
0x5d04   18    4    0    1    1
0x5d04   19    4    0    1    2
0x5d04   20    4    0    1    3
0x5d04   21    4    0    1    4
0x5d04   22    4    0    1    5
0x5d04   23    4    0    1    6
0x5d04   24    4    0    1    7
0x5d04   25    4    0    1    8
0x5d04   26    4    0    1    9
0x5d04   27    4    0    1   10
0x5d04   28    4    0    1   11
0x5d04   29    4    0    1   12
0x5d04   30    4    0    1   13
0x5d04   31    4    0    1   14
0x5d04   32    4    0    1   15
0x5d04   33    4    0    2    0
0x5d04   34    4    0    2    1
0x5d04   35    4    0    2    2
0x5d04   36    4    0    2    3
0x5d04   37    4    0    2    4
0x5d04   38    4    0    2    5
0x5d04   39    4    0    2    6
0x5d04   40    4    0    2    7
0x5d04   41    4    0    2    8
0x5d04   42    4    0    2    9
0x5d04   43    4    0    2   10
0x5d04   44    4    0    2   11
0x5d04   45    4    0    2   12
0x5d04   46    4    0    2   13
0x5d04   47    4    0    2   14
0x5d04   48    4    0    2   15
0x5d05    1    5    0    0    0
0x5d05    2    5    0    0    1
0x5d05    3    5    0    0    2
0x5d05    4    5    0    0    3
0x5d05    5    5    0    0    4
0x5d05    6    5    0    0    5
0x5d05    7    5    0    0    6
0x5d05    8    5    0    0    7
0x5d05    9    5    0    0    8
0x5d05   10    5    0    0    9
0x5d05   11    5    0    0   10
0x5d05   12    5    0    0   11
0x5d05   13    5    0    0   12
0x5d05   14    5    0    0   13
0x5d05   15    5    0    0   14
0x5d05   16    5    0    0   15
0x5d05   17    5    0    1    0
0x5d05   18    5    0    1    1
0x5d05   19    5    0    1    2
0x5d05   20    5    0    1    3
0x5d05   21    5    0    1    4
0x5d05   22    5    0    1    5
0x5d05   23    5    0    1    6
0x5d05   24    5    0    1    7
0x5d05   25    5    0    1    8
0x5d05   26    5    0    1    9
0x5d05   27    5    0    1   10
0x5d05   28    5    0    1   11
0x5d05   29    5    0    1   12
0x5d05   30    5    0    1   13
0x5d05   31    5    0    1   14
0x5d05   32    5    0    1   15
0x5d05   33    5    0    2    0
0x5d05   34    5    0    2    1
0x5d05   35    5    0    2    2
0x5d05   36    5    0    2    3
0x5d05   37    5    0    2    4
0x5d05   38    5    0    2    5
0x5d05   39    5    0    2    6
0x5d05   40    5    0    2    7
0x5d05   41    5    0    2    8
0x5d05   42    5    0    2    9
0x5d05   43    5    0    2   10
0x5d05   44    5    0    2   11
0x5d05   45    5    0    2   12
0x5d05   46    5    0    2   13
0x5d05   47    5    0    2   14
0x5d05   48    5    0    2   15
##############################################################################
