#!/bin/bash

ofile=03_fwdet_lookup_sts.txt

cat <<EOF > $ofile
##############################################################################
# Lookup table for the TRB3 unpacker of the FwDet STS detector
# Format:
# trbnet-address  channel  module  layer  cell  subcell
##############################################################################
[StsTrb3Lookup]
// Parameter Context: StsTrb3LookupProduction
//----------------------------------------------------------------------------
EOF

cat lookup/sts1_lay{0,1,2,3}.txt >> $ofile
cat lookup/sts2_lay{0,1,2,3}.txt >> $ofile

cat <<EOF >> $ofile
##############################################################################
EOF
