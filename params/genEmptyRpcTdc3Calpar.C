/////////////////////////////////////////////////////////////////////////
//  Take from Ora and write to ASCII 	

{
    /////////////////////////////////////////////////////////////////
    //Change info!!!!
    TString *outFile = new TString("91_fwdet_rpc_trb3calpar.txt");

    TString *author = new TString("Rafal Lalik");
    TString *desc   = new TString("FwDet-Rpc params for straw");
    //////////////////////////////////////////////////////////////////
    // file info
    TString *run = new TString("be11224145351.hld"); // file from aug 2011

    //Int_t runId = 113153631;  //  file aug011
    Int_t runId = 192717397;//89973347;  // file nov2010
    ///////////////////////////////////////////////////////////////////

    // Setup
    Hades *myHades = new Hades;
    HRuntimeDb* rtdb=gHades->getRuntimeDb();

    HSpectrometer* spec=gHades->getSetup();

    Int_t nFwDetMods[10]     = {1,1,0,0,0,0,1,0,0,0};
    spec->addDetector(new HFwDetDetector);
    spec->getDetector("FwDet")->setModules(-1,nFwDetMods);

    const Int_t nsubevts = 2;
    Int_t tdc_addresses[nsubevts][5] = {
            { 0x8c00, 3, 0x6800, 0x6810, 0x6811 },
            { 0x8c01, 3, 0x6820, 0x6830, 0x6831 }
    };

    const Int_t edges = 1;
    const Int_t nchans = 49;
    const Int_t nbins = 512;
    const Float_t binval = 0.0;
    const Int_t ndata = nchans * nbins * (edges == 3 ? 2 : 1);

    Float_t * darr = new Float_t[ndata];
    for (int i = 0; i < ndata; ++i)
            darr[i] = binval;
    HFRpcTrb3Calpar* pCalpar = new HFRpcTrb3Calpar;

    printf("Adding %d TDCs\n", nsubevts);
    for (Int_t i = 0; i < nsubevts; ++i)
    {
        for (int j = 0; j < tdc_addresses[i][1]; ++j)
        {
            printf("Adding  SubEvt 0x%x  TDC 0x%x\n", tdc_addresses[i][0], tdc_addresses[i][2+j]);
            HTrb3CalparTdc * tdc = pCalpar->addTdc(tdc_addresses[i][2+j]);
            tdc->makeArray(tdc_addresses[i][0], edges, nchans, nbins);
            tdc->fillArray(darr, ndata);
        }
    }
    rtdb->addContainer(pCalpar);

    HParAsciiFileIo* out=new HParAsciiFileIo;
    if(!out->open(outFile->Data(),"out") ) {
        delete myHades;
        return -1;
    }
    pCalpar->write(out);
//     rtdb->setOutput(out);
//     rtdb->writeContainers();
//     rtdb->printContainers();
    delete myHades;
}
