#!/bin/bash

# Submission script for GridEngine (GE). Each job will 
# be executed via the jobScript.sh
# This jobScript supports up to 7 parameters. Edit 
# the user specific part of the script according to 
# your program.
#
# Input to the script is a filelist with 1 file per line.
# For each file a job is started. With the parameter 
# nFilesPerJob a comma separated filelist will be 
# generated and handed to the job script. This feature
# is usefull when running many small jobs. Each
# job has its own logfile. All needed directories for the
# logfiles will be created if non existing.
#
# IMPORTANT: the hera/prometheus cluster jobs will only
# see the /hera file system. All needed scripts, programs
# and parameters have to be located on /hera or the job
# will crash. This script syncs your working dir to the submission
# dir on /hera . Make sure your scripts use the submission dir!
# Software should be taken from /cvmfs/hades.gsi.de/install/
#
# job log files will be named like inputfiles. If nFilesPerJob > 1
# the log files will contain the partnumber.
#
######################################################################
#   CONFIGURATION

user=$(whoami)
currentDir=$(pwd | xargs -i basename {})
currentDir=../$currentDir

submissionbase=/lustre/hades/user/${user}/sub/feb22

######################################################################

#---------------------------------------------------------------------
# create needed dirs
if [ ! -d $submissionbase ]
then
    echo "===> CREATE SUBMISSIONBASEDIR : $submissionbase"
    mkdir -p $submissionbase
else
    echo "===> USE SUBMISSIONBASEDIR : $submissionbase"
fi

#---------------------------------------------------------------------
# sync the local modified stuff 
# to the submission dir
echo "===> SYNC CURENTDIR TO SUBMISSIONDIR : rsync  -vHaz $currentDir ${submissionbase}"
rsync  -vHaz --exclude={'feb21_dst_template/.git/*'} $currentDir ${submissionbase}/

syncStat=$?

if [ ! $syncStat -eq 0 ]
then
    echo "===> ERROR : SYNCHRONIZATION ENCOUNTERED PROBLEMS"
else
    echo "sync done!!"
fi
