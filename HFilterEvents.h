
#ifndef __HFilterEvents___


#include "hreconstructor.h"
#include "hcategorymanager.h"
#include "hcategory.h"
#include "hrecevent.h"


#include "hparticletool.h"
#include "hparticleevtinfo.h"
//--------category definitions---------



#include <iostream>
#include <cstdlib>
using namespace std;


class HFilterEvents : public HReconstructor
{

    Int_t nEvents;
    Int_t nEventsAccepted;

public:

    HFilterEvents(const Text_t *name = "",const Text_t *title ="") {
	nEvents = 0;
	nEventsAccepted = 0;
    }
    virtual ~HFilterEvents() {;}

    Bool_t init()
    {
        return kTRUE;
    }

    Int_t execute()
    {
	nEvents++;

	HCategory* evtInfoCat = (HCategory*)HCategoryManager::getCategory(catParticleEvtInfo);
	HParticleEvtInfo* evtInfo=0;
	evtInfo = HCategoryManager::getObject(evtInfo,evtInfoCat,0 );

	if(evtInfo&&!evtInfo->isGoodEvent(Particle::kGoodTRIGGER|          // standard event selection apr12
					  Particle::kGoodVertexClust|
					  Particle::kGoodVertexCand|
					  Particle::kGoodSTART|
					  Particle::kNoPileUpSTART|
					  Particle::kNoVETO|
					  Particle::kGoodSTARTVETO|
					  Particle::kGoodSTARTMETA
					 ))
	{
	    return kSkipEvent;
	} else {
            nEventsAccepted++;
	    return 0;
	}

    }

    Bool_t finalize()
    {

	cout<<"HFilterEvents :: nEvents "<<nEvents<<" accepted "<<nEventsAccepted<<" ratio "<<  nEventsAccepted/(Float_t)nEvents<<endl;
	return kTRUE;
    }

};


#endif
